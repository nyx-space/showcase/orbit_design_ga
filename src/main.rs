/*
    Nyx, blazing fast astrodynamics
    Copyright (C) 2021 Christopher Rabotin <christopher.rabotin@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

extern crate nyx_space as nyx;
extern crate oxigen;

use nyx::md::ui::*;

use oxigen::prelude::*;

mod genotype;

use genotype::OrbitIndividual;

fn main() {
    let problem_size: usize = 4;
    let population_size = problem_size * 8;
    let log2 = (f64::from(problem_size as u32) * 4_f64).log2().ceil();
    let (solutions, generation, _progress, _population) =
        GeneticExecution::<f64, OrbitIndividual>::new()
            .population_size(population_size)
            .genotype_size(problem_size)
            .mutation_rate(Box::new(MutationRates::Linear(SlopeParams {
                start: f64::from(problem_size as u32) / (8_f64 + 2_f64 * log2) / 100_f64,
                bound: 0.005,
                coefficient: -0.0002,
            })))
            .selection_rate(Box::new(SelectionRates::Linear(SlopeParams {
                start: log2 - 2_f64,
                bound: log2 / 1.5,
                coefficient: -0.0005,
            })))
            .select_function(Box::new(SelectionFunctions::Cup))
            .run();

    println!("Finished in the generation {}", generation);
    let cosm = Cosm::de438();
    for (it, sol) in solutions.iter().enumerate() {
        // Propagate the trajectory
        let (_, traj) = sol.prop();
        // Convert to body fixed frame
        let bf_traj = traj
            .to_frame(cosm.frame("IAU Earth"), cosm.clone())
            .unwrap();
        // Save that trajectory with the ground track
        bf_traj
            .to_csv_with_groundtrack(&format!("output-{}.csv", it + 1), cosm.clone())
            .unwrap();
        sol.attempt(true);
    }
}
