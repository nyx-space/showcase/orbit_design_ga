extern crate lazy_static;
extern crate nyx_space as nyx;
extern crate oxigen;
extern crate rand;

use nyx::md::ui::*;
use nyx::od::ui::GroundStation;

use lazy_static::lazy_static;

use oxigen::prelude::*;

use rand::distributions::Standard;
use rand::prelude::*;

use std::fmt;
use std::sync::Arc;

lazy_static! {
    // Load the NASA NAIF DE438 planetary ephemeris.
    static ref COSM: Arc<Cosm> = Cosm::de438();
}

/// An individual stores both the genes and the actual orbit
/// Maybe it doesn't need a full orbit? Idk
#[derive(Clone)]
pub struct OrbitIndividual {
    genes: Vec<f64>,
}

impl OrbitIndividual {
    /// Returns the initial state and the output trajectory
    #[allow(clippy::identity_op)]
    pub fn prop(&self) -> (Orbit, Traj<Orbit>) {
        // Grab the shared Cosm reference, defined as a lazy_static
        let cosm = (*COSM).clone();
        // Grab the Earth Mean Equator J2000 frame
        let eme2k = cosm.frame("EME2000");
        // Set the initial start time of the scenario
        let epoch = Epoch::from_gregorian_tai_at_noon(2021, 2, 25);
        // Define the state with an altitude above the reference frame.
        // Nearly circular orbit (ecc of 0.01), inclination of 49 degrees and TA at 30.0
        let default_orbit = Orbit::keplerian_alt(500.0, 0.01, 49.0, 0.0, 0.0, 30.0, epoch, eme2k);
        // Modify it with the genes
        let orbit = default_orbit
            .with_ecc(default_orbit.ecc() + self.genes[0])
            .with_inc(default_orbit.inc() + self.genes[1])
            .with_raan(default_orbit.raan() + self.genes[2])
            .with_aop(default_orbit.aop() + self.genes[3]);

        // Let's specify the force model to be two body dynamics
        // And use the default propagator setup: a variable step Runge-Kutta 8-9
        let setup = Propagator::default(OrbitalDynamics::two_body());

        // Use the setup to seed a propagator with the initial state we defined above.
        let mut prop = setup.with(orbit);
        // Now let's propagate for a week and generate the trajectory so we can analyse it.
        let (_, traj) = prop.for_duration_with_traj(1 * TimeUnit::Day).unwrap();

        (orbit, traj)
    }

    /// From the initial state, propagate and return the fitness.
    /// As a separate function, we can call it on the solution that was found.
    pub fn attempt(&self, pretty_print: bool) -> f64 {
        let (orbit, traj) = self.prop();

        // Grab the shared Cosm reference, defined as a lazy_static
        let cosm = (*COSM).clone();

        // Define the landmark by specifying a name, a latitude, a longitude, an altitude (in km) and a frame.
        // Note that we're also "cloning" the Cosm: don't worry, it's a shared object, so we're just cloning the
        // the reference to it in memory, and never loading it more than once.
        let landmark = GroundStation::from_point(
            "Eiffel Tower".to_string(),
            48.8584,
            2.2945,
            0.0,
            cosm.frame("IAU Earth"),
            cosm.clone(),
        );

        // Let's keep track of the max elevation for each bucket:
        // 45-55; 55-65; 65-75; 75-85; 85-90
        // We'll store them in a vector
        let mut elevations = vec![0, 0, 0, 0, 0];

        // Iterate through the trajectory between the bounds.
        for state in traj.every(2 * TimeUnit::Minute) {
            // Compute the elevation
            let (elevation, _) = landmark.elevation_of(&state);
            let bucket_idx = if elevation >= 85.0 {
                0
            } else if elevation >= 75.0 {
                1
            } else if elevation >= 65.0 {
                2
            } else if elevation >= 55.0 {
                3
            } else if elevation >= 45.0 {
                4
            } else {
                // We don't care about this elevation
                continue;
            };
            if elevations[bucket_idx] == 0 {
                elevations[bucket_idx] += 1000;
            } else {
                elevations[bucket_idx] += 1
            }
        }

        let sum_fitness: u32 = elevations.iter().sum();
        if pretty_print {
            println!("{:o}", orbit);
            println!(
                "45-55: {}\t55-65: {}\t65-75: {}\t75-85: {}\t85-90: {}",
                if elevations[4] > 0 {
                    elevations[4] - 999
                } else {
                    0
                },
                if elevations[3] > 0 {
                    elevations[3] - 999
                } else {
                    0
                },
                if elevations[2] > 0 {
                    elevations[2] - 999
                } else {
                    0
                },
                if elevations[1] > 0 {
                    elevations[1] - 999
                } else {
                    0
                },
                if elevations[0] > 0 {
                    elevations[0] - 999
                } else {
                    0
                }
            );
            println!("Fitness: {}", sum_fitness);
        }

        f64::from(sum_fitness)
    }
}

impl fmt::Display for OrbitIndividual {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{:?}", self.genes)
    }
}

impl Genotype<f64> for OrbitIndividual {
    type ProblemSize = usize;

    fn iter(&self) -> std::slice::Iter<f64> {
        self.genes.iter()
    }
    fn into_iter(self) -> std::vec::IntoIter<f64> {
        self.genes.into_iter()
    }
    fn from_iter<I: Iterator<Item = f64>>(&mut self, genes: I) {
        self.genes = genes.collect();
    }

    fn generate(size: &Self::ProblemSize) -> Self {
        let mut individual = Vec::with_capacity(*size as usize);
        let mut rgen = SmallRng::from_entropy();
        for i in 0..*size {
            let delta: f64 = rgen.sample(Standard);
            let param_delta = if i == 0 {
                // We want to vary the eccentricity by very small numbers
                0.01 * delta
            } else {
                // But we want  to vary the other element by large numbers
                10.0 * delta
            };
            individual.push(param_delta);
        }
        Self { genes: individual }
    }

    fn fitness(&self) -> f64 {
        self.attempt(true)
    }

    fn mutate(&mut self, rgen: &mut SmallRng, index: usize) {
        let delta: f64 = rgen.gen();
        if index == 0 {
            self.genes[index] += 0.01 * delta;
        } else {
            self.genes[index] += 10.0 * delta;
        }
    }

    fn is_solution(&self, fitness: f64) -> bool {
        // We now have 5 buckets and the first hit is a thousands points.
        fitness >= 5000.0
    }
}
