# Orbit design via genetic algorithm
Walk through of this example available here: https://nyxspace.com/showcase/mission_design/orbit_design_ga/ .

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/nyx-space/showcase/orbit_design_ga)

# To execute:

```
cargo run --release
```